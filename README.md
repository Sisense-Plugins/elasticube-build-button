#	Elasticube Build Button

This article explains how to install and use the Elasticube Rebuild Button.

This plugin adds a button to all dashboards' settings menu, that enables an admin user the ability to trigger an Elasticube build.  This is an important distinction, because ONLY admin users are allowed to start Elasticube builds, so the button will not show up for viewer or designer users.

__Step 1__: Download the attached .zip file.

Extract the .zip file into the following path: "[Sisense installation path]\PrismWeb\Plugins". 

__Step 2__: Use the Settings Menu

On your dashboard, open the settings menu and click the new button for rebuilding the elasticube

![settings menu](screenshots/Menu.png)

This will use the Sisense REST API to kick off a build process, and will show you a notification with either a success or failure notification.

![success](screenshots/Build Started.png)

If you try and start the build of an Elasticube that is already building, you will get this notification instead 

![build in progress](screenshots/Build In Progress.png)

NOTES:
This new functionality is available ONLY for admin users

