
prism.run([function(){

	//	Init the notify framework
	prism.notify = new Notyf({
		delay: 2000
	});

	//	Helper function to figure out if a value exists within an array
	var isInArray = function(value, array) {
		return array.indexOf(value) > -1;
	}

	//	Send an alert popup for a given message
	var sendMessage = function(message,type){
		if (type == "success"){
			prism.notify.confirm(message)
		} else if (type == "error"){
			prism.notify.alert(message)
		}
	}

	//	Function used to settings a dashboard's style
	var rebuild = function(){

		//	Get a reference to the $http service
		var $http = prism.$injector.get('$http');

		//	Define the build options
		var buildType = 'entire',
			server = prism.$ngscope.dashboard.datasource.address,
			elasticube = prism.$ngscope.dashboard.datasource.title;

		//	Build the API endpoint for checking on the status of an Elasticube
		var checkUrl = '/api/elasticubes/servers/' + server + '/status?q=' + encodeURIComponent(elasticube);

		//	Check on the Elasticube first
		$http.get(checkUrl).then(function(response){

			var message = '',
				statusObj = response.data[0],
				status = statusObj ? statusObj.status : null;

			//	The list of status codes that indicate a cube is still building
			var isBuildingCodes = [16, 16384, 65536, 514, 2048];

			if (status == null) {
				//	Elasticube not found
				message = 'The Elasticube for this dashboard could not be found...';
				sendMessage(message,'error');
			} else if (isInArray(status,isBuildingCodes)) {
				//	Elasticube already buildiing
				message = 'The ' + statusObj.title + ' elasticube is already building.'
				sendMessage(message,'error');
			} else {
				//	Elasticube is ok to build,

				//	Build the API endpoint, using the options
				var buildUrl = '/api/elasticubes/' + server + '/' + elasticube + '/startBuild?type=' + buildType;

				//	Make the API call
				$http.post(buildUrl).then(function(response){

					//	Build started!
					var message = 'Build Started for ' + elasticube;
					sendMessage(message,'success')
				})
			}
		})
	}

	//	Function to determine if the user is allowed to rebuild the elasticube
	var canRebuild = function(){
		var isAdmin = (prism.user.userAuth.base.isAdmin) || (prism.user.userAuth.base.isSuper);
		return isAdmin;
	}

	//	Handler for adding in the dropdown menu
	prism.on('beforemenu',function(scope,args){

		//	Only run for a dashboard menu when the user is an admin
		var isDashboardMenu = ($$get(args, "settings.name", '') == "dashboard");
		var shouldOpen = isDashboardMenu && canRebuild();
		if (shouldOpen){

			//	Get the list of existing items
			var menuItems = args.settings.items;

			//	Create a simple separator
			var separator = {
				"type":"separator"
			}

			//	Create a new menu item
			var newItem = {
				"caption": "Rebuild Elasticube",
				"desc": "Trigger a build of the current Elasticube",
				"disabled": false,
				"execute": rebuild
			}

			//	Add new item to the menu
			menuItems.push(separator);
			menuItems.push(newItem);
		}	
	})
}])